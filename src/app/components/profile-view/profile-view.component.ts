import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/service/user/service.service';
import { environment } from 'src/environments/environment';
import { Usuario } from 'src/models/i.usuario';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss'],
})
export class ProfileViewComponent implements OnInit {
  
  public skills: string [] = [];
  public user: Usuario;

  @Input() IsEditable: boolean;

  constructor(private userService: ServiceService, private route : Router) {
    this.user = {
      id: '',
      perfil: '',
      correo : '',
      nombre: '',
      skills: this.skills,
      telefono: 0
    };
  }

  ngOnInit() { 
    this.skills = [];
    this.getProfile();
  }

  public keyValueEvent(skill) {
    console.log(skill);
    const tmp = skill;
    this.skills.push(tmp);
  }

  private getProfile(): void {
    // invoca desde profile-init
    if (!this.IsEditable) {
      // ir a firebase
      this.userService.getUser().subscribe(
        (user) => {
          console.log('user' , user);
          this.user = user;
          this.skills = this.user.skills;
        }
      );
    } else {
      this.user.correo = localStorage.getItem(environment.application.email);
      this.user.id = localStorage.getItem(environment.application.email);
      this.user.telefono = parseInt(localStorage.getItem(environment.application.phone));
    }
  }

  public setProfile(): void {
    this.user.skills = this.skills;
    console.log(this.user);
    this.userService.setUser(this.user).then(
      (smsSuccess) => {
        console.log('Data success service' , smsSuccess);
        localStorage.setItem(environment.application.isRegister , 'true');
        this.route.navigateByUrl('menu');
      }
    );
  }

  public removeChip(index: number): void {
    console.log(index);
    this.skills.splice(index , 1);
  }
}
