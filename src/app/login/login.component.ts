import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { Utils } from 'src/utilities/Utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  public correo: string;

  constructor( private route : Router, public alertController: AlertController) { }

  ngOnInit() {
    this.correo = '';
  }

  public async butonSms(): Promise<void>{
    const isPhone = localStorage.getItem(environment.application.isRegister) || '';
    if (Utils.ValidateClearValue(this.correo)) {
      if (isPhone === ''  ) {
        localStorage.setItem(environment.application.email , this.correo);
        this.route.navigateByUrl('login-sms');
      } else {
        this.route.navigateByUrl('menu');
      }
    } else {
      const alert = await this.alertController.create({
        header: 'Error',
        message: 'Debes digitar un correo válido.',
        buttons: ['OK']
      });
      await alert.present();
    }
  }
}

