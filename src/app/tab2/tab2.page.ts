import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  
  public skills: string [] = [];

  constructor() {}

  ngOnInit(){
    this.skills = [];
  }
  public keyValueEvent(skill) {
    console.log(skill);
    const tmp = skill;
    this.skills.push(tmp);
    }
}
