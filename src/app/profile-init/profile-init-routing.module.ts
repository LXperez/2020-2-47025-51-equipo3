import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileInitPage } from './profile-init.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileInitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileInitPageRoutingModule {}
