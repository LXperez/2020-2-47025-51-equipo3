import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileInitPageRoutingModule } from './profile-init-routing.module';

import { ProfileInitPage } from './profile-init.page';
import { ProfileViewComponent } from '../components/profile-view/profile-view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileInitPageRoutingModule
  ],
  declarations: [ProfileInitPage, ProfileViewComponent],
  entryComponents: [ProfileViewComponent]
})
export class ProfileInitPageModule {}
