import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(private firestore: AngularFirestore) { }

  public getDataWithId(id: string , collection: string): Observable<any> {
    console.log('id ' , id);
    console.log('collection ' , collection);
    return this.firestore.collection(collection).doc(id).snapshotChanges().pipe(map(action => {
      console.log('playload ' ,action.payload.data());
      let data = action.payload.data();
      data['id'] = action.payload.id;
      return data ;
    }));
  }

  public setData(object: any , collection: string): Promise<any>{
    return this.firestore.collection(collection).doc(object.id).set(object);
  }
}
