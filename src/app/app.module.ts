import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginSMSComponent } from './login-sms/login-sms.component';
import { MessageComponent } from './message/message.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from 'src/environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseService } from './service/firebase/service.service';
import { ServiceService } from './service/user/service.service';

@NgModule({
  declarations: [AppComponent,
  LoginComponent,
  MessageComponent,
  LoginSMSComponent],
  entryComponents: [MessageComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule ,
    CommonModule ,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFirestore,
    FirebaseService,
    ServiceService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
