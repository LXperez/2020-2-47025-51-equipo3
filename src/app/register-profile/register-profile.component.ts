import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register-profile',
  templateUrl: './register-profile.component.html',
  styleUrls: ['./register-profile.component.scss'],
})
export class RegisterProfileComponent implements OnInit {

  public isEditable: boolean;

  constructor() { }

  ngOnInit() {}

}
