import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Usuario } from 'src/models/i.usuario';
import { Chat, Messages } from 'src/models/i.chat';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {

  public user: Usuario;
  public chat: Chat;
  public messageInput: string;

  constructor() { this.loadMessage(); }

  ngOnInit() {
    
  }

  private loadMessage(): void {
    const usuarioEmisor:Usuario  = {
      correo : "correo@correo.com",
      id: "7PVNmARstRAKQlQpkjhgfI",
      nombre: "Claudia doe",
      perfil : "Soy ing de sistemas",
      skills: [],
      telefono: 321456789
    }

    this.user  = {
      correo : "correo@correo.com",
      id: "7PVNmARstRAKQlQp53AI",
      nombre: "Jhon doe",
      perfil : "Soy ing de sistemas",
      skills: [],
      telefono: 321456789
    }

    let mensajes: Array<Messages> = [];
    for (let i = 0 ; i < 15 ; i ++) {
      if(i % 2 == 0){
        const msm: Messages = {
          UsuarioEmisor:this.user,
          fecha: new Date(),
          mensaje: "Mensaje " + i + " de " + this.user.nombre
        }
        mensajes.push(msm);
      } else {
        const msm: Messages = {
          UsuarioEmisor: usuarioEmisor,
          fecha: new Date(),
          mensaje: "Mensaje " + i + " de " + usuarioEmisor.nombre
        }
        mensajes.push(msm);
      }
    }

    this.chat = {
      estado : 1 ,
      fechaFin : null,
      fechaInicio: new Date(),
      mensajes, 
      usuarioEmisor,
      usuarioReceptor: this.user
    }

    console.log('chat' , this.chat);
  }

  public sendMessage(): void {
    const msm: Messages = {
      UsuarioEmisor: this.user,
      fecha: new Date(),
      mensaje: this.messageInput
    }

    this.chat.mensajes.push(msm);
  }

}
