import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { Utils } from 'src/utilities/Utils';

@Component({
  selector: 'app-login-sms',
  templateUrl: './login-sms.component.html',
  styleUrls: ['./login-sms.component.scss'],
})
export class LoginSMSComponent implements OnInit {

  public phoneNumber: string;

  constructor(private route : Router, public alertController: AlertController) { }

  ngOnInit() {
    this.phoneNumber = '';
  }
  public async butonMenu(): Promise<void>{
    if (Utils.ValidateClearValue(this.phoneNumber)) {
      localStorage.setItem(environment.application.phone , this.phoneNumber);
      this.route.navigateByUrl('profile-init');
    } else {
      const alert = await this.alertController.create({
        header: 'Error',
        message: 'Debes digitar un teléfono válido.',
        buttons: ['OK']
      });
      await alert.present();
    }
  }
}
