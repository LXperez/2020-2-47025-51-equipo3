export const environment = {
  production: true,
  firebaseConfig : {
    apiKey: 'AIzaSyAK-QqvEbMjAw-ZWuutUbSq9tJvLzwlo7M',
    authDomain: 'makuruma-3136a.firebaseapp.com',
    databaseURL: 'https://makuruma-3136a.firebaseio.com',
    projectId: 'makuruma-3136a',
    storageBucket: 'makuruma-3136a.appspot.com',
    messagingSenderId: '695459882977',
    appId: '1:695459882977:web:50f6278533e706f4c5d4a0'
  },
  application : {
    phone: 'phone_user',
    email: 'email_user',
    isRegister: 'register_user'
  },
  collections: {
    user: 'Usuarios'
  }
};
