export interface Usuario {
    id: string;
    correo : string;
    nombre: string;
    perfil: string;
    skills: string[];
    telefono: number;
}

