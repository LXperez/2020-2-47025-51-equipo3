import { Usuario } from './i.usuario';

export interface Messages {
    fecha: Date;
    mensaje: string;
    UsuarioEmisor: Usuario;
}

export interface Chat {
    estado : number;
    fechaFin: Date;
    fechaInicio: Date;
    mensajes: Messages[];
    usuarioEmisor: Usuario;
    usuarioReceptor: Usuario;
}